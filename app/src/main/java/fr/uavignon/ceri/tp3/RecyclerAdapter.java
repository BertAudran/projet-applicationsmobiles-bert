package fr.uavignon.ceri.tp3;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import fr.uavignon.ceri.tp3.data.Relic;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp3.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp3.RecyclerAdapter.class.getSimpleName();
    private ListViewModel listViewModel;

    private static int nb=0;
    private ArrayList<Relic> relicList;
    private ArrayList<Relic> saveList;

    public static int getNb() {
        return nb;
    }

    public static void setNb(int nb) {
        RecyclerAdapter.nb = nb;
    }

    public void updateList(ArrayList<Relic> list){

        relicList=list;
        notifyDataSetChanged();
    }

    public void restoreList(){
        relicList=saveList;
        notifyDataSetChanged();
    }

    public void sortWorking(){
        if (relicList!=null) {
            relicList = saveList;
            Collections.sort(relicList, new Comparator<Relic>() {
                @Override
                public int compare(Relic o1, Relic o2) {
                    return Boolean.compare(o1.getWorking(),o2.getWorking());
                }
            });
            notifyDataSetChanged();
        }
    }

    public void sortBrand(){
        if (relicList!=null) {
            relicList = saveList;
            Collections.sort(relicList, new Comparator<Relic>() {
                @Override
                public int compare(Relic o1, Relic o2) {
                    return o1.getBrand().compareTo(o2.getBrand());
                }
            });
            notifyDataSetChanged();
        }
    }

    public void sortA(){
        if (relicList!=null) {
            relicList = saveList;
            Collections.sort(relicList, new Comparator<Relic>() {
                @Override
                public int compare(Relic o1, Relic o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            notifyDataSetChanged();
        }
    }

    public void sort1(){
        if (relicList!=null) {
            ArrayList<Relic> tmp = new ArrayList<>();
            for (int i = 0; i < relicList.size(); i++) {
                if (relicList.get(i).getYear() > 10) {
                    tmp.add(relicList.get(i));
                }
            }
            Collections.sort(tmp, new Comparator<Relic>() {
                @Override
                public int compare(Relic o1, Relic o2) {
                    return o1.getYear() - o2.getYear();
                }
            });
            relicList = tmp;
            notifyDataSetChanged();
        }
    }

    public ArrayList<Relic> getSaveList() {
        return saveList;
    }

    public ArrayList<Relic> getRelicList() {
        return relicList;
    }

    public void setSaveList(ArrayList<Relic> saveList) {
        this.saveList = saveList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v=null;
        if(nb%2==0) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }else{
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout2, viewGroup, false);
        }
        nb++;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(relicList.get(i).getName());
        viewHolder.itemDetail.setText(relicList.get(i).getDescription());
        if (relicList.get(i).getPicture()!=null || relicList.get(i).getPicture()!=""){
            Glide.with(viewHolder.itemView.getContext())
                    .load(relicList.get(i).getPicture())
                    .into(viewHolder.itemIcon);
        }
        /*
        if (relicList.get(i).getSmallIconUri() == null)
            viewHolder.itemIcon.setImageResource(0);
        else
            viewHolder.itemIcon.setImageDrawable(viewHolder.itemIcon.getResources().getDrawable(viewHolder.itemIcon.getResources().getIdentifier(relicList.get(i).getSmallIconUri(),
                    null, viewHolder.itemIcon.getContext().getPackageName())));

         */
    }

    @Override
    public int getItemCount() {
        //return Book.books.length;
        return relicList == null ? 0 : relicList.size();
    }

    public void setRelicList(ArrayList<Relic> relics) {
        relicList = relics;
        notifyDataSetChanged();
    }


    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    private void deleteItem(String id) {
        if (listViewModel != null)
            listViewModel.deleteCity(id);
    }

    public void setRelicsList(ArrayList<Relic> relics) {
        saveList=relics;
        relicList=relics;
        notifyDataSetChanged();
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        TextView itemTemp;
        ImageView itemIcon;

         ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);
            //itemTemp = itemView.findViewById(R.id.item_temp);
            itemIcon = itemView.findViewById(R.id.item_image);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.tp3.RecyclerAdapter.this.deleteItem(idSelectedLongClick);
                            Snackbar.make(itemView, "Ville supprimée !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        case R.id.menu_update:
                           // ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment("lol");
                           // action.setCityNum(idSelectedLongClick);
                            //Navigation.findNavController(itemView).navigate(action);
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.relicList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections  .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    action.setItemNum(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.relicList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }




     }

}