package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Relic;

@Database(entities = {Relic.class}, version = 11, exportSchema = false)
public abstract class RelicRoomDatabase extends RoomDatabase {

    private static final String TAG = RelicRoomDatabase.class.getSimpleName();

    public abstract RelicDao relicDao();

    private static RelicRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static RelicRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RelicRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    RelicRoomDatabase.class,"relic_database")
                                    .fallbackToDestructiveMigration()
                                    .build();



                            // with populate
                    /*
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    RelicRoomDatabase.class,"relic_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();
                    */
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        RelicDao dao = INSTANCE.relicDao();

                        //dao.deleteAll();
                    });

                }
            };



}
