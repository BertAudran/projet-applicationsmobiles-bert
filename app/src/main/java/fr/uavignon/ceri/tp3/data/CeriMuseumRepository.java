package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.RelicDao;
import fr.uavignon.ceri.tp3.data.database.RelicRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.CeriMuseumInterface;
import fr.uavignon.ceri.tp3.data.webservice.ItemResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import fr.uavignon.ceri.tp3.data.database.RelicRoomDatabase;

import static fr.uavignon.ceri.tp3.data.database.RelicRoomDatabase.databaseWriteExecutor;


public class CeriMuseumRepository {

    private static final String TAG = CeriMuseumRepository.class.getSimpleName();


    private MutableLiveData<Relic> selectedRelic=new MutableLiveData<>();;

    private final CeriMuseumInterface api;

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private RelicDao relicDao;

    private MutableLiveData<String> version=new MutableLiveData<>();

    volatile int nbAPILoads=0;


    private MutableLiveData<ArrayList<Relic>> relics=new MutableLiveData<>();
    private MutableLiveData<ArrayList<String>> categories=new MutableLiveData<>();

    public MutableLiveData<ArrayList<Relic>> getRelics() {
        return relics;
    }

    private static volatile CeriMuseumRepository INSTANCE;

    public synchronized static CeriMuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CeriMuseumRepository(application);
        }

        return INSTANCE;
    }

    public CeriMuseumRepository(Application application) {
        RelicRoomDatabase db = RelicRoomDatabase.getDatabase(application);
        relicDao = db.relicDao();
        // = relicDao.getAllCities();
        //selectedCity = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CeriMuseumInterface.class);
        /*System.out.println("LA");
        System.out.println("STATE :"+Environment.getExternalStorageState());
        try{
            File file = new File( Environment.getExternalStorageDirectory(),"/version.txt");
            if (file.exists()){
                System.out.println("LOL");
                Scanner myReader = new Scanner(file);
                version.setValue(myReader.nextLine());
                myReader.close();
                System.out.println("VERSION BDD:"+version.getValue());
            }
            else{
                System.out.println("LEL");

                FileWriter writer = new FileWriter(file);
                System.out.println("LAL");
                writer.write("00000");
                writer.close();
            }
            System.out.println("ICI");
        }
        catch (IOException e) {
            System.out.println("FAILED");
            e.printStackTrace();
        }
*/
    }




    public MutableLiveData<Relic> getSelectedRelic() {
        return selectedRelic;
    }

    public void loadWeatherAllCities(){
        //List<City> cities= relicDao.getSynchrAllCities();
        //nbAPILoads=cities.size();
        /*for (int i=0;i<cities.size();i++){
            loadCollection();
        }*/
        //nbAPILoads=allCities.getValue().size();
        /*
        for (int i=0;i<allCities.getValue().size();i++){
            loadWeatherCity(allCities.getValue().get(i));
        }

         */
    }

public void categoriesList(){
    loadCollectionFromDatabase();
    String delims = "[;]";
    for (int i=0;i<relics.getValue().size();i++){

        String[] tokens = relics.getValue().get(i).getCategories().split(delims);
        for (int j = 0; j < tokens.length; j++)
            if (!categories.getValue().contains(tokens[j])){
                categories.getValue().add(tokens[j]);
            }
    }

}

    public void removeAll(){
        System.out.println("SUPP");
        relicDao.deleteAll();
        relics.postValue(null);
    }


    public void loadCollectionFromDatabase(){
        ArrayList<Relic> allRelics= (ArrayList<Relic>) relicDao.getSynchrAllRelics();
        relics.postValue(allRelics);
    }


    public void checkVersion(){
        isLoading.postValue(Boolean.TRUE);
        if (relics.getValue()!=null && relics.getValue().size()>0){
            version.postValue(relics.getValue().get(0).getVersion());
        }
        else{
            version.postValue("");
        }
        api.getVersion().enqueue(
                new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call,
                                           Response<String> response) {
                        System.out.println(response.body()+" VS "+version.getValue());

                        if (!response.body().equals(version.getValue())){
                            System.out.println("MISE A JOUR");
                            version.setValue(response.body());
                            loadCollection();
                        }
                        else {
                            isLoading.postValue(Boolean.FALSE);
                            System.out.println("DEJA A JOUR");
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });
    }

    public void loadCollection(){
        //isLoading.postValue(Boolean.TRUE);
        //api.getWeather(q,key);
        //final MutableLiveData<WeatherResponse> result = new MutableLiveData<>();

        //checkVersion();
        //System.out.println(version);

        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {
                        //Log.d("API",response.body().toString());
                        ArrayList<Relic> relicTMP=new ArrayList<>();
                        int i=0;
                        for (String key: response.body().keySet()) {
                            //System.out.println(key + "=" + response.body().get(key).name);
                            //System.out.println(response.body().get(key).working);
                            //relicTMP.add(new Relic(key,response.body().get(key).name,response.body().get(key).description));
                            ItemResult.transferInfo(response.body().get(key), key, relicTMP,version.getValue());
                            //System.out.println(relicTMP.get(i).getWorking());
                            long res=insertRelic(relicTMP.get(i));
                            //System.out.println("INSERTION:"+res);
                            i=i+1;
                            //getRelic(0);

                        }

                        relics.setValue(relicTMP);
                        /*
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        */
                        isLoading.postValue(Boolean.FALSE);
                        /*
                        if (relics.getValue()!=null){
                            System.out.println("LISTE :");
                            for (int i=0;i<relics.getValue().size();i++){
                                System.out.println(relics.getValue().get(i).getName());
                            }
                        }
                        else{
                            System.out.println("List null");
                        }

                         */
                       // System.out.println(selectedRelic.getValue().getName());
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d("FAILURE API",t.getMessage());
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });

    }


    public long insertRelic(Relic newRelic) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return relicDao.insert(newRelic);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //if (res != -1)
         //   selectedCity.setValue(newRelic);
        return res;
    }

    public int updateCity(Relic relic) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return relicDao.update(relic);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedRelic.setValue(relic);
        return res;
    }

    public void deleteCity(String id) {
        databaseWriteExecutor.execute(() -> {
            relicDao.deleteRelic(id);
        });
    }

    public void getRelic(String id)  {

        Future<Relic> frelic = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return relicDao.getRelicById(id);
        });
        try {
            selectedRelic.setValue(frelic.get());
           //System.out.println(frelic.get().getName());
            // System.out.println(selectedRelic.getValue().getName());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
