package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Relic;

public class ListFragment extends Fragment {

    public static final String TAG = ListFragment.class.getSimpleName();

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progress;
    private EditText searchBar;
    private Button sortButton;
    private Button categoriesButton;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);


        listenerSetup();
        //viewModel.loadCollection();
        observerSetup();


    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);
        searchBar=getView().findViewById(R.id.searchText) ;
        sortButton=getView().findViewById(R.id.sortButton);
        categoriesButton=getView().findViewById(R.id.categories);

        //sortList();

        FloatingActionButton fab = getView().findViewById(R.id.fab);
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changesSortList();
            }
        });
        categoriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.ListFragment.this)
                        .navigate(R.id.action_ListFragment_to_categoryFragment);
            }
        });
    }

    private void sortList() {
        System.out.println("Sorted:"+Args.getSorted());
        if (Args.getSorted()=="A"){
            adapter.sortA();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="1"){
            adapter.sort1();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="Brand"){
            adapter.sortBrand();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="Working"){
            adapter.sortWorking();
            sortButton.setText(Args.getSorted());
        }
    }

    private void changesSortList() {
        if (Args.getSorted() =="Trier"){

            Args.setSorted("A");
            adapter.sortA();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="A"){

            Args.setSorted("1");
            adapter.sort1();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="1"){
            Args.setSorted("Brand");
            adapter.sortBrand();
            sortButton.setText(Args.getSorted());
        }
        else if (Args.getSorted()=="Brand"){
            Args.setSorted("Working");
            adapter.sortWorking();
            sortButton.setText(Args.getSorted());
        }
        else {
            Args.setSorted("Trier");
            adapter.restoreList();
            sortButton.setText(Args.getSorted());
        }
        System.out.println(Args.getSorted());
    }

    public  void filter(String text){
        if (adapter.getSaveList()!=null) {
            ArrayList<Relic> temp = new ArrayList();
            for (Relic d : adapter.getSaveList()) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getName().contains(text)) {
                    temp.add(0, d);
                } else if (d.getDescription().contains(text)) {
                    temp.add(d);
                } else if (d.getBrand().contains(text)) {
                    temp.add(d);
                }
            }
            //update recyclerview
            adapter.updateList(temp);
        }
    }

    private void observerSetup() {
        /*
        viewModel.getAllCities().observe(getViewLifecycleOwner(),
                cities -> adapter.setCityList(cities));
*/
        viewModel.getAllRelics().observe(getViewLifecycleOwner(),
                relics -> {
            adapter.setNb(0);
            adapter.setRelicsList(relics);
            sortList();
        });


        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
        );
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                System.out.println("Changé");
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });


    }

}