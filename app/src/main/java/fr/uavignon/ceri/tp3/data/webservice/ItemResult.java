package fr.uavignon.ceri.tp3.data.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.uavignon.ceri.tp3.data.Relic;

public class ItemResult {





    public static void transferInfo(ItemResponse body, String key,ArrayList<Relic> relics, String version) {

        Relic relic = new Relic();
        relic.setId(key);
        relic.setName(body.name);
        relic.setDescription(body.description);
        relic.setWorking(body.working);
        if (body.year!=null){
            relic.setYear(body.year);
        }
        if (body.brand!=null){
            relic.setBrand(body.brand);
        }
        if (body.pictures!=null){
            int i=0;
            for (String k:body.pictures.keySet()) {
                if (i==0){
                    relic.setPicture("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+key+"/images/"+k);
                }
                i+=1;
                //System.out.println(k);
            }
        }
        if (body.categories!=null){
            String cat="";
            for (int i=0;i<body.categories.size();i++){
                cat=cat+body.categories.get(i)+"\n";
            }
            relic.setCategories(cat);
        }
        if (body.technicalDetails!=null){
            String det="";
            for (int i=0;i<body.technicalDetails.size();i++){
                det=det+body.technicalDetails.get(i)+"\n";
                //System.out.println(body.technicalDetails.get(i));
            }
            relic.setDetails(det);

        }
        Date date=new Date();
        date.setHours(date.getHours()+2);
        if (!version.isEmpty()){
            relic.setVersion(version);
        }
        //LocalDate date = LocalDate.now(ZoneId.of("Europe/Paris"));
        relic.setLastUpdate(new SimpleDateFormat("dd/MM/yyyy  HH:mm").format(date));
        //relic.setCategories(body.categories);
        //System.out.println(relic.getName()+"->"+relic.getWorking());
        relics.add(relic);

    }

}
