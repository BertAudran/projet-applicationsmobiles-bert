package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Relic;

@Dao
public interface RelicDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Relic relic);

    @Query("DELETE FROM relic_database")
    void deleteAll();


    /*
    @Query("SELECT * from relic_table ORDER BY name ASC")
    LiveData<List<City>> getAllCities();
    */
    /*
    @Query("SELECT * from relic_table ORDER BY name ASC")
    List<City> getSynchrAllCities();
     */

    @Query("SELECT * from relic_database ORDER BY name ASC")
    List<Relic> getSynchrAllRelics();

    @Query("DELETE FROM relic_database WHERE _id = :id")
    void deleteRelic(String id);

    @Query("SELECT * FROM relic_database WHERE _id = :id")
    Relic getRelicById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Relic relic);
}
