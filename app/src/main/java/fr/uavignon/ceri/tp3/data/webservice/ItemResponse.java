package fr.uavignon.ceri.tp3.data.webservice;

import org.w3c.dom.Text;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemResponse {
    public final Boolean working=false;
    public final String description=null;
    public final String name=null;
    public final Integer year=null;
    public final String brand=null;

    public final List<String> categories=null;


    public final Map<String,String> pictures=null;

    public final List<String> technicalDetails=null;

}
