package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.CeriMuseumRepository;
import fr.uavignon.ceri.tp3.data.Relic;

public class ListViewModel extends AndroidViewModel {
    private CeriMuseumRepository repository;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<String> sorted;
    private MutableLiveData<ArrayList<Relic>> allRelics;

    public ListViewModel (Application application) {
        super(application);
        //repository.loadCollection();
        //ArrayList<Relic> relics=new ArrayList<Relic>();
        allRelics=new MutableLiveData<>();
        sorted=new MutableLiveData<>();
        sorted.setValue("Trier");
        repository = CeriMuseumRepository.get(application);
        isLoading=repository.isLoading;
        /*
        for (int i=0;i<allRelics.getValue().size();i++){
            System.out.println(allRelics.getValue().get(i).getName());
        }
        */
        allRelics=repository.getRelics();
    }

    public MutableLiveData<String> getSorted() {
        return sorted;
    }

    public void setSorted(String sorted) {
        this.sorted.setValue(sorted);
    }

    public void deleteCity(String id) {
        repository.deleteCity(id);
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Relic>> getAllRelics() {
        return allRelics;
    }

    public void loadCollection(){
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.checkVersion();

            }
        };
        t.start();

    }

    public void loadCollectionFromDatabase(){
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDatabase();

            }
        };
        t.start();

    }
    public void removeAll(){

        Thread t = new Thread(){
            public void run(){
                repository.removeAll();

            }
        };
        t.start();

    }

}
