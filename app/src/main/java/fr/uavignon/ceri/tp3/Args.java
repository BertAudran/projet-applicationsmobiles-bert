package fr.uavignon.ceri.tp3;

public class Args {
    private static String sorted="Trier";

    public static String getSorted() {
        return sorted;
    }

    public static void setSorted(String sorted) {
        Args.sorted = sorted;
    }
}
