package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName = "relic_database", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Relic {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description=null;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand="Inconnu";

    @NonNull
    @ColumnInfo(name="working")
    private boolean working=false;

    @NonNull
    @ColumnInfo(name="year")
    private int year=-1;


    @NonNull
    //@TypeConverters(DataConverter.class)
    @ColumnInfo(name="categories")
    private String categories="";



    @NonNull
    @ColumnInfo(name="picture")
    private String picture="";

    @NonNull
    @ColumnInfo(name="details")
    private String details="";

    @NonNull
    @ColumnInfo(name="lastUpdate")
    private String lastUpdate="";

    @NonNull
    @ColumnInfo(name="version")
    private String version="";

    @NonNull
    public String getVersion() {
        return version;
    }

    public void setVersion(@NonNull String version) {
        this.version = version;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(@NonNull String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @NonNull
    public String getDetails() {
        return details;
    }

    public void setDetails(@NonNull String details) {
        this.details = details;
    }

    @NonNull
    public String getCategories() {
        return categories;
    }

    public void setCategories(@NonNull String categories) {
        this.categories = categories;
    }


    @NonNull
    public String getPicture() {
        return picture;
    }

    public void setPicture(@NonNull String picture) {
        this.picture = picture;
    }


    public Relic(String id, String name, String description) {
        this.id=id;
        this.name=name;
        this.description=description;
    }

    public Relic() {

    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /*
    @NonNull
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(@NonNull List<String> categories) {
        this.categories = categories;
    }
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getWorking() {
        return working;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand=brand;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }
}
