package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.CeriMuseumRepository;
import fr.uavignon.ceri.tp3.data.Relic;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<Boolean> isLoading;
    private CeriMuseumRepository repository;
    private MutableLiveData<Relic> relic;
    private  MutableLiveData<Throwable> webServiceThrowable;


    public DetailViewModel (Application application) {
        super(application);
        isLoading=new MutableLiveData<Boolean>();
        repository = CeriMuseumRepository.get(application);
        isLoading=repository.isLoading;
        webServiceThrowable=repository.webServiceThrowable;
        relic = new MutableLiveData<>();
    }

    public void setRelic(String id) {
        repository.getRelic(id);
        relic = repository.getSelectedRelic();
        //System.out.println(relic.getValue().getName());
    }
    LiveData<Relic> getRelic() {
        return relic;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}


}

