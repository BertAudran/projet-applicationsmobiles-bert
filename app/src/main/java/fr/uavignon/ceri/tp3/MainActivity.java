package fr.uavignon.ceri.tp3;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;

import com.google.android.material.snackbar.Snackbar;


public class MainActivity extends AppCompatActivity {

    //private WeatherRepository repository;

    /*public MainActivity(Application application){
        repository=WeatherRepository.get(application);
    }*/

    ListViewModel listViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listViewModel.loadCollectionFromDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.menu_main, menu);
        /*
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

            
         */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Mise à jour de la collection...",
                    Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();


            listViewModel.loadCollection();
            //View view=findViewById(android.R.id.content);
            System.out.println("FINI");

            return true;
        }
        if (id == R.id.action_remove_all) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Suppression de la collection",
                    Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();


            listViewModel.removeAll();
            //View view=findViewById(android.R.id.content);
            System.out.println("FINI");

            return true;
        }




        return super.onOptionsItemSelected(item);
    }
}