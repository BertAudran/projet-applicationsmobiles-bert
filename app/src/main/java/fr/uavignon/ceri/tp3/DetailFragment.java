package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textRelicName, textDescription, textTemperature, textHumidity, textCloudiness, textWind, textLastUpdate,textDetails, labelDetails;
    private ImageView imgWeather;
    private ProgressBar progress;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String relicID = args.getItemNum();
        Log.d(TAG,"selected id="+relicID);
        viewModel.setRelic(relicID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textRelicName = getView().findViewById(R.id.nameCity);
        textDescription = getView().findViewById(R.id.country);
        textTemperature = getView().findViewById(R.id.editTemperature);
        textHumidity = getView().findViewById(R.id.editHumidity);
        textCloudiness = getView().findViewById(R.id.editCloudiness);
        textWind = getView().findViewById(R.id.editWind);
        textDetails=getView().findViewById(R.id.editDetails);
        textLastUpdate = getView().findViewById(R.id.lastUpdate);

        imgWeather = getView().findViewById(R.id.iconeWeather);
        labelDetails=getView().findViewById(R.id.labelDetails);
        progress = getView().findViewById(R.id.progress);

        /*getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();


                //viewModel.updateWeather();
            }
        });
*/
        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getRelic().observe(getViewLifecycleOwner(),
                relic -> {
                    if (relic != null) {
                        Log.d(TAG, "observing relic view");

                        textRelicName.setText(relic.getName());
                        textDescription.setText(relic.getDescription());
                        if(relic.getWorking()==true){
                            textTemperature.setText("Fonctionnel");
                        }
                        else{
                            textTemperature.setText("Non Fonctionnel");
                        }
                        if(relic.getYear()==-1 || relic.getYear()==0){
                            textHumidity.setText("Inconnu");
                        }
                        else{
                            textHumidity.setText(String.valueOf(relic.getYear()));
                        }
                        textWind.setText(relic.getBrand());

                        if (relic.getPicture()!=null && relic.getPicture()!=""){
                            Glide.with(this)
                                   .load(relic.getPicture())
                                    .into(imgWeather);
                        }
                        if (relic.getCategories()!=null){
                            textCloudiness.setText(relic.getCategories());
                        }
                        if (relic.getDetails()!=null && relic.getDetails()!="" && !relic.getDetails().isEmpty()){
                            System.out.println("Details:"+relic.getDetails());
                            textDetails.setText(relic.getDetails());
                        }
                        else{
                            labelDetails.setVisibility(View.INVISIBLE);
                            System.out.println("Inivisible");
                        }
                        if (relic.getLastUpdate()!=null && !relic.getLastUpdate().isEmpty()){
                            textLastUpdate.setText(relic.getLastUpdate());
                        }
                        else{
                            textLastUpdate.setVisibility(View.INVISIBLE);
                        }
                        /*
                        if (city.getTemperature() != null)
                            textTemperature.setText(Math.round(city.getTemperature())+" °C");
                        if (city.getHumidity() != null)
                            textHumidity.setText(city.getHumidity()+" %");
                        if (city.getCloudiness() != null)
                            textCloudiness.setText(city.getCloudiness()+" %");
                        if (city.getFullWind() != null)
                            textWind.setText((city.getFullWind()));
                        textLastUpdate.setText(city.getStrLastUpdate());
                         */
                        // set ImgView
                        /*
                        if (city.getIconUri() != null)
                            imgWeather.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(city.getIconUri(),
                                null, getContext().getPackageName())));

                         */
                    }
                });

    viewModel.getIsLoading().observe(getViewLifecycleOwner(),
            isLoading ->{
                if(isLoading){
                    progress.setVisibility(View.VISIBLE);
                }
                else{
                    progress.setVisibility(View.GONE);
                }
            }
            );
    viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }


}